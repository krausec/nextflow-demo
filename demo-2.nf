#!/usr/bin/env nextflow

csvs = Channel.fromPath('/data/wkdv/input/*.csv')

process mean {
  input:
  file csv from csvs

  output:
  stdout result

  module 'foss/2018b'
  module 'R/3.6.0'

  time '60s'
  clusterOptions '-l h_vmem=1G -binding linear:1'

  script:
  """
  #!/usr/bin/env Rscript

  numbers <- as.numeric(readLines("$csv"))
  mean <- mean(numbers)

  cat(paste0(mean, "\n"))
  """
}

result.println { "mean: $it" }
