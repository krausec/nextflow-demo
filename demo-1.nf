#!/usr/bin/env nextflow

csvs = Channel.fromPath('/data/wkdv/input/*.csv')

process mean {
  input:
  file csv from csvs

  output:
  stdout result

  time '60s'
  clusterOptions '-l h_vmem=1G -binding linear:1'

  script:
  """
  echo $csv
  """
}

result.println { "file: $it" }
