#!/usr/bin/env nextflow

csvs = Channel.fromPath('/data/wkdv/input/*.csv')

process mean {
  input:
  file csv from csvs

  output:
  stdout result

  module 'foss/2018b'
  module 'R/3.6.0'

  time '60s'
  clusterOptions '-l h_vmem=1G -binding linear:1'

  script:
  """
  #!/usr/bin/env Rscript

  numbers <- as.numeric(readLines("$csv"))
  mean <- mean(numbers)

  cat(paste0(mean, "\n"))
  """
}

means = result.collectFile(name: 'means.csv')

process plot {
  input:
  file means

  output:
  file 'means.png' into pngplot

  module 'foss/2018b'
  module 'R/3.6.0'

  time '60s'
  clusterOptions '-l h_vmem=1G -binding linear:1'

  script:
  """
  #!/usr/bin/env Rscript

  numbers <- as.numeric(readLines("$means"))

  png("means.png")
  plot(numbers)
  dev.off()
  """
}

pngplot.println { "plot: $it" }
